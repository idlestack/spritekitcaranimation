//
//  ViewController.swift
//  CustomLaunchScreen
//
//  Created by Douglas Spencer on 6/20/17.
//  Copyright © 2017 Douglas Spencer. All rights reserved.
//

import UIKit
import SpriteKit

class MainVC: UIViewController {
    
    var scene:CarScene?
    @IBOutlet weak var SKView: SKView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        self.scene = CarScene(size: CGSize(width: self.SKView.frame.width, height: self.SKView.frame.height))
        self.SKView.presentScene(scene)
    }

    

}

