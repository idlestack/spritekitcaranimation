//
//  CarScene.swift
//  CustomLaunchScreen
//
//  Created by Douglas Spencer on 6/20/17.
//  Copyright © 2017 Douglas Spencer. All rights reserved.
//

import UIKit
import SpriteKit

class CarScene: SKScene {
    
    var CarFrame : [SKTexture]?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init( size: CGSize) {
        super.init(size: size)
        self.backgroundColor = UIColor.white
        var frames : [SKTexture] = []
        
        let cars = SKTextureAtlas(named: "Car")
        
        for index in 1...6 {
            let tName = "car\(index)"
            let texture = cars.textureNamed(tName)
            frames.append(texture)
        }
        
        CarFrame = frames
        
        RunCar()
        
        
    }
    
    func RunCar() {
        let texture = CarFrame?[0]
        
        let car: SKSpriteNode = SKSpriteNode(texture: texture)
        
        car.size = CGSize(width: 150, height: 150)
        
        car.position = CGPoint(x: 0, y: 100)
        
        self.addChild(car)
        
        car.run(SKAction.repeatForever(SKAction.animate(with: CarFrame!, timePerFrame: 0.05, resize: false, restore: true)))
        
        let DistanceToTravel = self.frame.width + car.size.width
        
        let time = TimeInterval(abs(DistanceToTravel / 150))
        
        let moveAction = SKAction.moveBy(x: DistanceToTravel, y: 0, duration: time)
        let removeaction = SKAction.run { 
            car.removeAllActions()
            car.removeFromParent()
        }
        
        let allActions  = SKAction.sequence([moveAction,removeaction])
        
        car.run(allActions)
        
    }

}
